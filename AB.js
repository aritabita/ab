const Discord = require('discord.js');
const client = new Discord.Client();
const config = require('./config.json');
const fs = require('fs');
const database = require('./database/connect');
const Money = database.import('./models/money');
const cron = require('node-cron');
const Rent = database.import('./models/rent');
client.commands = new Discord.Collection();
const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));
for (const file of commandFiles) {
	const command = require(`./commands/${file}`);
	client.commands.set(command.name, command);
}
const cooldowns = new Discord.Collection();
const blacklist = [ 'ooc', 'character', 'test', 'search', 'chat', 'info', 'information', 'contest'];
const w = 'WWW';
const com = 'COM';
const http = 'HTTP';
const https = 'HTTPS';
cron.schedule('0 0 0 * * * *', async function() {
	const results = await Rent.findAll();
	results.forEach(async result => {
		if (result.days < 0) return;
		result.days = result.days - 1;
		await result.save();
		const searchedChannel = client.channels.get(result.roomID);
		if (!searchedChannel) return;
		if (!result.topic)
		{
			searchedChannel.setTopic(`This room has ${result.days} days left.`);
		}
		else {
			searchedChannel.setTopic(`${result.topic} || This room has ${result.days} days left.`);
		}
		if (result.days === 0)
		{
			const logchannel = client.channels.get(config.rent_log);
			logchannel.send(`❌ <@${result.ownerID}>'s room ${searchedChannel.name} was deleted.`);
			searchedChannel.delete();
		}
		else if (result.days === 1)
		{
			searchedChannel.send(`Dawn of the Final Day, <@${result.ownerID}>! Type /rraddtime ###(number of days) to add more days!`);

		}
	});
});
client.login(config.token);
client.on('ready', () => {
	console.log(`Arita has been booted up! Arita operates in ${client.guilds.size} servers, and for ${client.users.size} people!`);
});
client.on('error', (e) => console.error(e));
client.on('warn', (e) => console.warn(e));
client.on('debug', (e) => console.info(e));
client.on('message', async message =>{
	if (message.content.startsWith(config.prefix + 'eval')) {
		if(message.author.id !== config.ownerID) return;
		const args = message.content.split(' ').slice(1);
		try {
			message.delete();
			const code = args.join(' ');
			let evaled = eval(code);
			// if (typeof evaled !== 'string')
			evaled = require('util').inspect(evaled);
			message.channel.send(clean(evaled), { code:'xl' });
		} catch (err) {
			message.channel.send(`\`ERROR\` \`\`\`xl\n${clean(err)}\n\`\`\``);
		}
	}

	else if (message.channel.id == config.main_chat || message.channel.id == config.lewd_chat || message.channel.id == config.conversation_chat)
	{
		const msgArray = message.content.split(' ');
		const userName = await Money.findOne({ where: { userId: message.author.id } });
		if (!userName) {
			try {
				await Money.create({
					userId: message.author.id,
					gems: 1,
				});
			}
			catch(e) {
				return console.log(e);
			}
		}
		else {
			userName.increment('gems', { by: config.general_gain });
		}
		if (message.member.roles.has(config.adminID) || message.member.roles.has(config.modID)) return;

		for(let i = 0; i < msgArray.length; i++) {
			const index = msgArray[i].toUpperCase();
			if(index.indexOf(w + '.') !== -1 && index.indexOf('.' + com) !== -1) {
				message.delete();
				message.author.send(`Arita's words echo through your mind..."Hey! ${message.author}! No links allowed in main!"`);
			}
			else if(index.indexOf(http) !== -1 || index.indexOf(https) !== -1) {
				if(index.indexOf(w + '.') || index.indexOf('.' + com)) {
					message.delete();
					message.author.send(`Arita's words echo through your mind..."Hey! ${message.author}! No links allowed in main!"`);
				}
			}
		}
	}
	else if (config.image_subchannel.includes(message.channel.parentID) && (message.attachments.size) || (config.image_subchannel.includes(message.channel.parentID)) && (message.content.startsWith('https://'))) {
		const userName = await Money.findOne({ where: { userId: message.author.id } });
		if (!userName) {
			try {
				await Money.create({
					userId: message.author.id,
					gems: 1,
				});
			}
			catch(e) {
				return console.log(e);
			}
		}
		else {
			userName.increment('gems', { by: config.image_gain });
		}
	}
	else if (config.rp_subchannel.includes(message.channel.parentID) && (!message.content.startsWith('(')))
	{
		if (message.content.startsWith(config.prefix)) return commands(message);
		let loopOver = false;
		for(let i = 0; i < blacklist.length; i++) {
			if(message.channel.name.includes(blacklist[i]))
			{
				loopOver = true;
				return;
			}
		}
		if (loopOver) return;
		const count = message.content.split(' ').length;
		const userName = await Money.findOne({ where: { userId: message.author.id } });
		const moneyTotal = count * config.rp_word_gain;
		if (!userName) {
			try {
				await Money.create({
					userId: message.author.id,
					gems: 1,
				});
			}
			catch(e) {
				return console.log(e);
			}
		}
		else {
			userName.increment('gems', { by: moneyTotal });
		}
	}
	else if (config.casual_subchannel.includes(message.channel.parentID))
	{
		let loopOver = false;
		for(let i = 0; i < blacklist.length; i++) {
			if(message.channel.name.includes(blacklist[i]))
			{
				loopOver = true;
				return;
			}
		}
		if (loopOver) return;
		const userName = await Money.findOne({ where: { userId: message.author.id } });
		if (!userName) {
			try {
				await Money.create({
					userId: message.author.id,
					gems: 1,
				});
			}
			catch(e) {
				return console.log(e);
			}
		}
		else {
			userName.increment('gems', { by: config.varied_gain });
		}
	}
	else if (message.channel.parentID == config.staff || message.channel.parentID == config.operators)
	{
		const userName = await Money.findOne({ where: { userId: message.author.id } });
		if (!userName) {
			try {
				await Money.create({
					userId: message.author.id,
					gems: 1,
				});
			}
			catch(e) {
				return console.log(e);
			}
		}
		else {
			userName.increment('gems', { by: config.staff_gain });
		}
	}
	else {
		if (!message.content.startsWith(config.prefix) || message.author.bot) return;
		commands(message);
	}
});

function commands(message) {
	const args = message.content.slice(config.prefix.length).split(/ +/);
	const commandName = args.shift().toLowerCase();

	const command = client.commands.get(commandName)
	|| client.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName));
	if (!command) return;
	if (command.args && !args.length) {
		let reply = message.channel.send(`You haven't provided any arguments, ${message.author}!`);

		if (command.usage) {
			reply += `\nThe proper usage of this command would be: \`${config.prefix}${command.name} ${command.usage}\``;
		}
		return message.channel.send(reply);
	}
	if(!cooldowns.has(command.name)) {
		cooldowns.set(command.name, new Discord.Collection());
	}
	const now = Date.now();
	const timestamps = cooldowns.get(command.name);
	const cooldownAmount = (command.cooldown || 3) * 1000;
	if (!timestamps.has(message.author.id)) {
		timestamps.set(message.author.id, now);
		setTimeout(() => timestamps.delete(message.author.id), cooldownAmount);
	}
	else{
		const expirationTime = timestamps.get(message.author.id) + cooldownAmount;
		if (now < expirationTime) {
			const timeLeft = (expirationTime - now) / 1000;
			return message.reply(`please wait at least ${timeLeft.toFixed(1)} more second(s) before reusing the \`${command.name}\` command!`);
		}
		timestamps.set(message.author.id, now);
		setTimeout(() => timestamps.delete(message.author.id), cooldownAmount);
	}
	try{
		command.execute(message, args);
	}
	catch (error) {
		console.error(error);
		message.reply('something has gone wrong when executing that command!');
	}
}

function clean(text) {
	if (typeof (text) === 'string')
	{return text.replace(/`/g, '`' + String.fromCharCode(8203)).replace(/@/g, '@' + String.fromCharCode(8203));}
	else{
		return text;
	}
}
