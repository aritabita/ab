##Presenting AB, A Discord bot for currency systems and overall fun-time madness!

**Note: This project was created over a year ago but is now being uploaded to a public repository**

AB contains different tools to make the user experience at the server a smoother one:

- Tailored specifically for Discord.
- Unique progression system tied to each member's ID.
- The ability for users to get rewards depending on their progression with the in-server currency of Gems.
- Built-in trading functionalities, allowing members to help each other grow, with trading being unlocked after a certain amount of progression.
- Support for different event functionalities such as April Fools or Anniversary events.
- Unity game interactions through sockets. (CURRENTLY ON HOLD)

---

Thank you for checking out this bot! Have a nice day
