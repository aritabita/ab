const Sequelize = require('sequelize');
const sequelize = new Sequelize({
	dialect: 'sqlite',
	logging: false,
	operatorsAliases: false,
	// SQLite only
	storage: 'money.sqlite',
});
sequelize.sync();
