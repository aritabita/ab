const database = require('../database/connect');
const Money = database.import('../models/money');
const config = require('../config');
const Rent = database.import('../models/rent');
const channelNameRegex = /^[\w-]+$/;

module.exports = {
	name: 'rent',
	aliases: ['ren', 'rnt'],
	description: 'Creates a Rented Room owned by the user for the specified amount of time.',
	usage: '<Room-name> <Days>',
	forUser: true,
	cooldown: 5,
	async execute(message, args) {
		if (message.channel.type !== 'text') return;
		if (!message.channel.parentID == config.serverEssentials.includes(message.channel.parentID)) return;
		message.delete();
		if (typeof args[0] == 'undefined') return message.reply('invalid command syntax.');
		const rentedDays = Math.round(args[1]);
		if (!channelNameRegex.test(args[0])) return message.reply('Only alphanumeric characters allowed!');
		if (args[0].length > 30) return message.reply('Channel names may be up to 30 characters long!');
		if (isNaN(rentedDays)) return message.reply('Invalid command syntax.');
		if (rentedDays <= 1) return message.reply('Number of days cannot be 1 or under!');
		// Gets time until next day, gives discount based on that.
		const date = new Date();
		const hours = date.getHours() + 1;
		const gemstogive = ((hours / 24) * 5);
		const daydiscount = Math.round(gemstogive);
		const price = rentedDays * config.rent_cost - daydiscount;
		const userName = await Money.findOne({ where: { userId: message.author.id } });
		if (userName) {
			if (userName.gems < price) return message.reply('Not enough gems!');
			const sentMessage = await message.channel.send(`This will cost ${price}${config.currency}. Are you sure?`);
			await sentMessage.react('✅');
			await sentMessage.react('❌');
			const filter = (reaction, user) => {
				return user.id === message.author.id
				&& (reaction.emoji.name === '✅' || reaction.emoji.name === '❌');
			};
			try{
				const collected = await sentMessage.awaitReactions(filter, { max: 1, time: 30000, errors: ['time'] });
				if (collected.first().emoji.name === '❌')
				{
					sentMessage.delete();
					return message.channel.send('Transaction cancelled');
				}

			}
			catch (error) {
				sentMessage.delete();
				return message.channel.send('User took too long to reply, transaction canceled.');
			}
			userName.decrement('gems', { by: price });
			sentMessage.delete();
		}
		message.guild.createChannel(args[0], 'text', [
			{
				id: message.author.id,
				allow: ['VIEW_CHANNEL', 'SEND_MESSAGES', 'MANAGE_MESSAGES'],
			},
			{
				id: message.guild.roles.get(config.modID),
				allow: ['VIEW_CHANNEL', 'SEND_MESSAGES', 'MANAGE_MESSAGES'],
			},
			{
				id: message.guild.roles.get(config.uncharted),
				deny: ['VIEW_CHANNEL'],
			},
			{
				id: message.guild.roles.get(config.dyno_role),
				allow: ['VIEW_CHANNEL', 'SEND_MESSAGES'],
			},
			{
				id: message.guild.defaultRole,
				deny: ['VIEW_CHANNEL'],
			}]).then(async channel => {
			channel.setParent(config.rent_cat);
			channel.setTopic(`This room has ${rentedDays} days left.`);
			channel.send(config.rented_welcome);
			const logchannel = message.guild.channels.get(config.rent_log);
			logchannel.send(`✅ ${message.author.username} created ${channel.name} for ${rentedDays} days.`);
			try {
				await Rent.create({
					ownerID: message.author.id,
					roomID: channel.id,
					days: rentedDays,
				});
				return message.reply(`${channel.toString()} has been created for ${rentedDays} days!`);
			}
			catch (e) {
				console.log(e);
				return message.reply('Something has gone wrong! Oh noes!');
			}
		});
	},
};
