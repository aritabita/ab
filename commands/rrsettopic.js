const database = require('../database/connect');
const config = require('../config');
const Rent = database.import('../models/rent');

module.exports = {
	name: 'rrsettopic',
	description: 'Rented Room owner only. Allows the Owner to change the room\'s Topic.',
	aliases: ['rrst', 'rrsett'],
	forUser: true,
	cooldown: 5,
	async execute(message, args) {
		if (message.channel.type !== 'text') return;
		message.delete();
		if (message.channel.parentID != config.rent_cat) return message.reply('Not a Rented Room.');
		const owner = await Rent.findOne({ where: { roomID: message.channel.id } });
		if (!owner) return;
		if (owner.ownerID != message.author.id) return message.reply('Not the channel owner.');
		if (!args) return message.reply('Must contain a topic');
		const topic = args.join(' ');
		if (topic.length > 500) return message.reply('Topic is too long!');
		await message.channel.setTopic(`${topic} || This channel has ${owner.days} days left.`);
		owner.topic = topic;
		await owner.save();
		message.reply('Topic set!');
		const logchannel = message.guild.channels.get(config.rent_log);
		logchannel.send(`✏ ${message.author.username} changed ${message.channel.name}'s topic to ${message.channel.topic}`);
	},
};
