const database = require('../database/connect');
const Money = database.import('../models/money');
const config = require('../config');

module.exports = {
	name: 'give',
	aliases: ['giv', 'gv'],
	description: 'Gives selected amount of gems to the target user',
	usage: '<user> <amount>',
	forUser: true,
	cooldown: 5,
	async execute(message, args) {
		if (message.channel.type !== 'text') return;
		if (!message.channel.parentID == config.serverEssentials.includes(message.channel.parentID)) return;
		message.delete();
		if (!message.mentions.users.first()) return message.reply('you need to mention a user');
		if (message.member.roles.has(config.role0)) return message.reply('innocents are unable to use this command.');
		if (message.mentions.members.first().roles.has(config.role0)) return message.reply('you cannnot give gems to Innocents.');
		if (!isFinite(parseInt(args[1], 10))) return message.reply('invalid number');
		const payment = parseInt(args[1], 10);
		const mentionedUser = message.mentions.users.first().id;
		const userName = await Money.findOne({ where: { userId: message.author.id } });
		const targetUser = await Money.findOne({ where: { userId: mentionedUser } });
		if (!userName || !targetUser) return message.reply('no gems can be given at this time.');
		if (payment < 0) return message.reply('nope.');
		if (mentionedUser == message.author.id) return message.reply('you can\'t give gems to yourself!');
		if (userName.gems < payment) return message.reply('not enough gems!');
		await userName.decrement('gems', { by: payment });
		await targetUser.increment('gems', { by: payment });
		message.reply(`success, ${message.mentions.users.first()} has been given ${payment}${config.currency}`);
	},
};
