const database = require('../database/connect');
const Money = database.import('../models/money');
const config = require('../config');
module.exports = {
	name: 'balance',
	aliases: ['bal', 'bln'],
	description: 'Allows admins or mods to check another user\'s gem balance',
	usage: '<user>',
	forUser: true,
	cooldown: 5,
	async execute(message) {
		if (message.channel.type !== 'text') return;
		if (!message.channel.parentID == config.serverEssentials.includes(message.channel.parentID)) return;
		if (message.member.roles.has(config.adminID) || message.member.roles.has(config.modID))
		{
			message.delete();
			if (!message.mentions.users.first()) return message.reply('you must mention someone!');
			const argu = message.content.split(' ').slice(1);
			const mentionedUser = message.mentions.users.first().id;
			const userName = await Money.findOne({ where: { userId: mentionedUser } });
			return message.reply(`${argu[0]} has ${userName.gems.toFixed(2)}` + config.currency);
		}
	},
};
