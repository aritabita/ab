const config = require('../config');
module.exports = {
	name: 'lock',
	description: 'Admin and mod use only. To be used while server is experiencing a heavy raid.',
	forUser: false,
	cooldown: 5,
	execute(message) {
		if (message.channel.type !== 'text') return;
		message.delete();
		if (message.member.roles.has(config.adminID) || message.member.roles.has(config.modID)) {
			message.guild.channels.find('name', 'main_chat').overwritePermissions('296324926745608199',
				{
					SEND_MESSAGES: false,
				});
			message.guild.channels.find('name', 'staff_lounge').send('@here SERVER\'S UNDER HEAVY RAID, ALL UNCHARTED USERS HAVE BEEN MUTED. FIRE THE WOOSH BLASTER. SPARE NO ONE!');
		}
	},
};
