const database = require('../database/connect');
const config = require('../config');
const Rent = database.import('../models/rent');

module.exports = {
	name: 'rrsetchannel',
	description: 'Rented Room owner only. Sets the channel to one of the following modes: private, public, readonly, dark, nonhuman, kink, or fandom.',
	aliases: ['rrsc', 'rrsetc'],
	usage: '<Mode>',
	forUser: true,
	cooldown: 5,
	async execute(message, args) {
		if (message.channel.type !== 'text') return;
		if (message.channel.parentID != config.rent_cat) return message.reply('Not a Rented Room.');
		const owner = await Rent.findOne({ where: { roomID: message.channel.id } });
		if (!owner) return;
		if (owner.ownerID != message.author.id) return message.reply('Not the channel owner.');
		if (!args[0]) return message.reply('Must contain a name');
		if (args[0].toLowerCase() == 'private')
		{
			message.channel.overwritePermissions(message.guild.defaultRole, {
				VIEW_CHANNEL: false,
				SEND_MESSAGES: false,
			});
			message.channel.overwritePermissions(message.guild.roles.get(config.fandom), {
				VIEW_CHANNEL: false,
				SEND_MESSAGES: false,
			});
			message.channel.overwritePermissions(message.guild.roles.get(config.kink), {
				VIEW_CHANNEL: false,
				SEND_MESSAGES: false,
			});
			message.channel.overwritePermissions(message.guild.roles.get(config.dark), {
				VIEW_CHANNEL: false,
				SEND_MESSAGES: false,
			});
			message.channel.overwritePermissions(message.guild.roles.get(config.nonhuman), {
				VIEW_CHANNEL: false,
				SEND_MESSAGES: false,
			});
			return message.reply('Channel set.');
		}
		else if (args[0].toLowerCase() == 'public')
		{
			message.channel.overwritePermissions(message.guild.defaultRole, {
				VIEW_CHANNEL: null,
				SEND_MESSAGES: true,
			});
			message.channel.overwritePermissions(message.guild.roles.get(config.fandom), {
				VIEW_CHANNEL: true,
				SEND_MESSAGES: true,
			});
			message.channel.overwritePermissions(message.guild.roles.get(config.kink), {
				VIEW_CHANNEL: true,
				SEND_MESSAGES: true,
			});
			message.channel.overwritePermissions(message.guild.roles.get(config.dark), {
				VIEW_CHANNEL: true,
				SEND_MESSAGES: true,
			});
			message.channel.overwritePermissions(message.guild.roles.get(config.nonhuman), {
				VIEW_CHANNEL: true,
				SEND_MESSAGES: true,
			});
			return message.reply('Channel set.');
		}
		else if (args[0].toLowerCase() == 'readonly')
		{
			message.channel.overwritePermissions(message.guild.defaultRole, {
				VIEW_CHANNEL: null,
				SEND_MESSAGES: false,
			});
			message.channel.overwritePermissions(message.guild.roles.get(config.fandom), {
				VIEW_CHANNEL: true,
				SEND_MESSAGES: false,
			});
			message.channel.overwritePermissions(message.guild.roles.get(config.kink), {
				VIEW_CHANNEL: true,
				SEND_MESSAGES: false,
			});
			message.channel.overwritePermissions(message.guild.roles.get(config.dark), {
				VIEW_CHANNEL: true,
				SEND_MESSAGES: false,
			});
			message.channel.overwritePermissions(message.guild.roles.get(config.nonhuman), {
				VIEW_CHANNEL: true,
				SEND_MESSAGES: false,
			});
			return message.reply('Channel set.');
		}
		else if (args[0].toLowerCase() == 'dark')
		{
			message.channel.overwritePermissions(message.guild.defaultRole, {
				VIEW_CHANNEL: false,
				SEND_MESSAGES: false,
			});
			message.channel.overwritePermissions(message.guild.roles.get(config.fandom), {
				VIEW_CHANNEL: false,
				SEND_MESSAGES: false,
			});
			message.channel.overwritePermissions(message.guild.roles.get(config.kink), {
				VIEW_CHANNEL: false,
				SEND_MESSAGES: false,
			});
			message.channel.overwritePermissions(message.guild.roles.get(config.dark), {
				VIEW_CHANNEL: true,
				SEND_MESSAGES: true,
			});
			message.channel.overwritePermissions(message.guild.roles.get(config.nonhuman), {
				VIEW_CHANNEL: false,
				SEND_MESSAGES: false,
			});
			return message.reply('Channel set.');
		}
		else if (args[0].toLowerCase() == 'nonhuman')
		{
			message.channel.overwritePermissions(message.guild.defaultRole, {
				VIEW_CHANNEL: false,
				SEND_MESSAGES: false,
			});
			message.channel.overwritePermissions(message.guild.roles.get(config.fandom), {
				VIEW_CHANNEL: false,
				SEND_MESSAGES: false,
			});
			message.channel.overwritePermissions(message.guild.roles.get(config.kink), {
				VIEW_CHANNEL: false,
				SEND_MESSAGES: false,
			});
			message.channel.overwritePermissions(message.guild.roles.get(config.dark), {
				VIEW_CHANNEL: false,
				SEND_MESSAGES: false,
			});
			message.channel.overwritePermissions(message.guild.roles.get(config.nonhuman), {
				VIEW_CHANNEL: true,
				SEND_MESSAGES: true,
			});
			return message.reply('Channel set.');
		}
		else if (args[0].toLowerCase() == 'kink')
		{
			message.channel.overwritePermissions(message.guild.defaultRole, {
				VIEW_CHANNEL: false,
				SEND_MESSAGES: false,
			});
			message.channel.overwritePermissions(message.guild.roles.get(config.fandom), {
				VIEW_CHANNEL: false,
				SEND_MESSAGES: false,
			});
			message.channel.overwritePermissions(message.guild.roles.get(config.kink), {
				VIEW_CHANNEL: true,
				SEND_MESSAGES: true,
			});
			message.channel.overwritePermissions(message.guild.roles.get(config.dark), {
				VIEW_CHANNEL: false,
				SEND_MESSAGES: false,
			});
			message.channel.overwritePermissions(message.guild.roles.get(config.nonhuman), {
				VIEW_CHANNEL: false,
				SEND_MESSAGES: false,
			});
			return message.reply('Channel set.');
		}
		else if (args[0].toLowerCase() == 'fandom')
		{
			message.channel.overwritePermissions(message.guild.defaultRole, {
				VIEW_CHANNEL: false,
				SEND_MESSAGES: false,
			});
			message.channel.overwritePermissions(message.guild.roles.get(config.fandom), {
				VIEW_CHANNEL: true,
				SEND_MESSAGES: true,
			});
			message.channel.overwritePermissions(message.guild.roles.get(config.kink), {
				VIEW_CHANNEL: false,
				SEND_MESSAGES: false,
			});
			message.channel.overwritePermissions(message.guild.roles.get(config.dark), {
				VIEW_CHANNEL: false,
				SEND_MESSAGES: false,
			});
			message.channel.overwritePermissions(message.guild.roles.get(config.nonhuman), {
				VIEW_CHANNEL: false,
				SEND_MESSAGES: false,
			});
			return message.reply('Channel set.');
		}
		else {return message.reply('Channel can only be set to private or public.');}
	},
};
