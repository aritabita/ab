const database = require('../database/connect');
const Money = database.import('../models/money');
const config = require('../config');

module.exports = {
	name: 'upgrade',
	aliases: ['upg'],
	description: `Allows the user to trade ${config.currency} for a rank upgrade. New upgrades might have benefits such as extra channel access and command unlocking. For more information, check the pinned messages in bot commands.`,
	forUser: true,
	cooldown: 5,
	async execute(message) {
		if (message.channel.type !== 'text') return;
		if (!message.channel.parentID == config.serverEssentials.includes(message.channel.parentID)) return;
		message.delete();
		const userName = await Money.findOne({ where: { userId: message.author.id } });
		if (message.member.roles.has(config.role[0]))
		{
			return message.reply('heeeey! Wait a second! You\'re already at max rank! There\'s not much that I can do at this time. But if you get enough Gems, who knows, maybe I\'ll have another surprise for you!');
		}
		else if (message.member.roles.has(config.role[1]))
		{
			if (userName.gems < config.upgradePrice[1])
			{
				const difference = config.upgradePrice[1] - userName.gems;
				message.reply(`not enough ${config.currency}! About ${difference.toFixed(2)}${config.currency} more should do.`);
			}
			else {
				await userName.decrement('gems', { by: config.upgradePrice[1] });
				message.member.addRole(config.role[0]);
				message.member.removeRole(config.role[1]);
				return message.reply(`congratulations! You have reached the maximun role! By this amazing prowess, you prove not only your loyalty to ${message.guild.name}, but your master of writing! Arita salutes you!`);
			}
		}
		else {
			for (let i = 0; i <= config.role.length; i++) {
				if (!message.member.roles.has(config.role[i])) continue;
				const difference = config.upgradePrice[i] - userName.gems;
				if (userName.gems < config.upgradePrice[i]) return message.reply(`not enough ${config.currency}! About ${difference.toFixed(2)}${config.currency} more should do.`);
				message.member.addRole(config.role[i - 1]);
				message.member.removeRole(config.role[i]);
				await userName.decrement('gems', { by: config.upgradePrice[i] });
				return message.reply('there you go! Congratulations on the approval! I\'m looking forward seeing you again!');
			}
		}
	},
};
