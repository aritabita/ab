const config = require('../config');
module.exports = {
	name: 'lift',
	description: 'Admin and mod use only. Lifts lockdown caused by /lock.',
	forUser: false,
	cooldown: 5,
	execute(message) {
		if (message.channel.type !== 'text') return;
		message.delete();
		if (message.member.roles.has(config.adminID) || message.member.roles.has(config.modID)) {
			message.guild.channels.find('name', 'main_chat').overwritePermissions('296324926745608199',
				{
					SEND_MESSAGES: true,
				});
			message.guild.channels.find('name', 'staff_lounge').send('@here Raid lockdown lifted. The Death Tally is gigantic. Be wary of introductions for the next hour');
		}
	},
};
