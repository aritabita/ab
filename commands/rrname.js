const database = require('../database/connect');
const config = require('../config');
const Rent = database.import('../models/rent');
const channelNameRegex = /^[\w-]+$/;

module.exports = {
	name: 'rrname',
	description: 'Rented Room owner only. Allows the Owner to change the Rented Room\'s name.',
	aliases: ['rrn'],
	usage: '<Room-name>',
	forUser: true,
	async execute(message, args) {
		if (message.channel.type !== 'text') return;
		message.delete();
		if (message.channel.parentID != config.rent_cat) return message.reply('Not a Rented Room.');
		const owner = await Rent.findOne({ where: { roomID: message.channel.id } });
		if (!owner) return;
		if (owner.ownerID != message.author.id) return message.reply('Not the channel owner.');
		const currentName = message.channel.name;
		if (!args[0]) return message.reply('Must contain a name');
		if (!channelNameRegex.test(args[0])) return message.reply('Only alphanumeric characters allowed!');
		if (args[0].length > 30) return message.reply('Channel names may be up to 30 characters long!');
		await message.channel.setName(args[0]);
		await message.reply('name set.');
		const logchannel = message.guild.channels.get(config.rent_log);
		logchannel.send(`📝 ${message.author.username} changed ${currentName}'s name to ${message.channel.name}.`);
	},
};
