const database = require('../database/connect');
const Money = database.import('../models/money');
const config = require('../config');
module.exports = {
	name: 'gems',
	aliases: ['gem', 'gm', 'money'],
	description: 'Checks user\'s Gem balance',
	forUser: true,
	cooldown: 5,
	async execute(message) {
		if (message.channel.type !== 'text') return;
		if (!message.channel.parentID == config.serverEssentials.includes(message.channel.parentID)) return;
		message.delete();
		const userName = await Money.findOne({ where: { userId: message.author.id } });
		if (!userName) return message.reply(`you have no ${config.currency}.`);
		return message.reply(`you have ${userName.gems.toFixed(2)}` + config.currency);
	},
};
