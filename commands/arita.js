const config = require('../config.json');
module.exports = {
	name: 'arita',
	aliases: ['ari', 'art', 'praise'],
	description: 'Praises Empress',
	forUser: true,
	cooldown: 5,
	execute(message) {
		if (!message.channel.parentID == config.serverEssentials.includes(message.channel.parentID)) return;
		message.delete();
		message.channel.send('Arita shall rule forever');
	},
};
