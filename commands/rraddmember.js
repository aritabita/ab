const database = require('../database/connect');
const config = require('../config');
const Rent = database.import('../models/rent');

module.exports = {
	name: 'rraddmember',
	description: 'Rented Room owner only. Allows an Owner to add any member to a Rented Room (Effect is only visible if Room is set to either Private or Read-Only).',
	aliases: ['rram'],
	usage: '<User>',
	forUser: true,
	async execute(message) {
		if (message.channel.type !== 'text') return;
		message.delete();
		if (message.channel.parentID != config.rent_cat) return message.reply('Not a Rented Room.');
		const owner = await Rent.findOne({ where: { roomID: message.channel.id } });
		if (!owner) return;
		if (owner.ownerID != message.author.id) return message.reply('Not the channel owner.');
		if (!message.mentions.members.first()) return message.reply('Must specify a user.');
		await message.channel.overwritePermissions(message.mentions.members.first(), {
			VIEW_CHANNEL: true,
			SEND_MESSAGES: true,
		});
		message.reply(`Welcome ${message.mentions.members.first()}`);
	},
};
