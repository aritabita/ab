module.exports = (sequelize, DataTypes) =>{
	return sequelize.define('pranks', {
		ownerID: {
			type: DataTypes.STRING,
			unique: true,
		},
		bomb: DataTypes.INTEGER,
	});
};
